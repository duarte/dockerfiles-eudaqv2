# 
# eudaq Dockerfile
# https://github.com/duartej/dockerfiles/eudaq
#
# Creates the environment to run the EUDAQ 
# framework. Uses as phusion image (based on 
# ubuntu-22.04 and the root-6.30.00 compiled 
# against C++17 standard
#

FROM phusion/baseimage:jammy-1.0.2
LABEL author="jorge.duarte.campderros@cern.ch" \ 
    version="3.0" \ 
    description="Docker image for EUDAQ-2 framework with C++17 support and ROOT 6.30.04"

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

# Place at the directory
WORKDIR /eudaq

# Install all dependencies
RUN apt-get update \ 
  && install_clean --no-install-recommends software-properties-common \ 
  && install_clean --no-install-recommends \ 
   # Pre-requisites for eudaq (https://github.com/eudaq/eudaq)
   build-essential \
   cmake \
   openssh-server \ 
   qtbase5-dev \
   qt5-qmake \
   xterm \
   zlib1g-dev \
   python3-dev \ 
   python3-numpy \
   wget \
   git \ 
   # Requisites for IPBus and uHAL packages (TLU producer)
   erlang \
   g++ \
   # libboost-all-dev \ --> Using 1.77 (system version is 1.74)
   libpugixml-dev \
   python-all-dev \
   pybind11-dev \ 
   python3-pybind11 \
   # IF EUDET TLU v1 is needed
   libusb-dev \ 
   libusb-1.0 \ 
   # Extra packages, not sure if needed
   python3-click \ 
   python3-pip \ 
   python3-matplotlib \
   python3-tk \
   python3-setuptools \
   python3-wheel \
   pkgconf \ 
   vim \ 
   g++ \
   gcc \
   gfortran \
   binutils \
   libxpm4 \ 
   libxft2 \ 
   libtiff5 \ 
   libtbb-dev \ 
   sudo \ 
  && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ROOT:: MOVE TO c++-17 support. Extract from the official corryvreckan image
COPY --from=rootproject/root:6.30.04-ubuntu22.04 /opt/root /rootfr/root

ENV ROOTSYS /rootfr/root
# BE aware of the ROOT libraries
ENV LD_LIBRARY_PATH $ROOTSYS/lib
ENV PYTHONPATH $ROOTSYS/lib

# download and compile BOOST dependency
RUN mkdir -p /_prov \
    && mkdir -p /eudaq/boost \
    && cd /_prov \
    && wget https://boostorg.jfrog.io/artifactory/main/release/1.77.0/source/boost_1_77_0.tar.gz \
    && tar xzf boost_1_77_0.tar.gz \
    && cd boost_1_77_0 \ 
    && ./bootstrap.sh --prefix=/eudaq/boost \
    && ./b2 install \ 
    && rm -rf /_prov

# Download and compile ipbus software
ENV LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/opt/cactus/lib:/eudaq/boost/lib"
ENV PATH="$PATH:${ROOTSYS}/bin:/opt/cactus/bin"

COPY fix_rebar_config rebar.config 

# The installation is performed in /opt/cactus
RUN git clone --depth=1 -b v2.8.9 --recurse-submodules https://github.com/ipbus/ipbus-software.git \ 
  && mv -f rebar.config ipbus-software/controlhub/rebar.config \
  && cd ipbus-software \
  # ip-bus need the pybind11 sources to be in there
  #&& ln -s /usr/include/pybind11 /eudaq/ipbus-software/uhal/python/pybind11 \
  && ln -s /usr/bin/python3 /usr/bin/python \
  && make EXTERN_BOOST_INCLUDE_PREFIX=/eudaq/boost/include EXTERN_BOOST_LIB_PREFIX=/eudaq/boost/lib PYTHON=/usr/bin/python3 \
  && make install 

# download the code, checkout the release and compile
# This will be used only for production!
RUN git clone -b master --single-branch https://github.com/duartej/eudaq.git \
  # And prepare the eudet tlu dependencies folder (not present) and CAEN
  && mkdir -p /eudaq/eudaq/user/tlu/extern \
  && mkdir -p /eudaq/eudaq/user/caen-dt5742/extern

# ADD suppport C++17 (not working when trying to pass it thourgh cmake command line_)
COPY CMakeLists-user-tlu-hardware.txt /eudaq/eudaq/user/tlu/hardware/CMakeLists.txt
# COPY The needed files for the TLU v0 (EUDET type)
COPY ZestSC1.tar.gz /eudaq/eudaq/user/tlu/extern/ZestSC1.tar.gz
COPY tlufirmware.tar.gz /eudaq/eudaq/user/tlu/extern/tlufirmware.tar.gz

# COPY The need files for the CAEN digitizer and install them
# Plus the relevant numpy we need
COPY CAENVMELib-v3.4.2.tgz /eudaq/eudaq/user/caen-dt5742/extern
COPY CAENComm-v1.6.0.tgz /eudaq/eudaq/user/caen-dt5742/extern
COPY CAENDigitizer-v2.17.3.tgz /eudaq/eudaq/user/caen-dt5742/extern
RUN cd /eudaq/eudaq/user/caen-dt5742/extern \
  && git clone https://github.com/duartej/CAENpy \
  && cd CAENpy \
  && pip install . \
  && pip install --force-reinstall -v "numpy==1.24.3"

# Untar files and continue with the compilation
RUN cd /eudaq/eudaq/user/tlu \ 
  && tar xzf extern/ZestSC1.tar.gz -C extern && rm extern/ZestSC1.tar.gz \
  && tar xzf extern/tlufirmware.tar.gz -C extern && rm extern/tlufirmware.tar.gz \
  && cd /eudaq/eudaq/user/caen-dt5742 \
  && tar xzf extern/CAENVMELib-v3.4.2.tgz -C extern && rm extern/CAENVMELib-v3.4.2.tgz \
  && cd extern/CAENVMELib-v3.4.2/lib && ./install_x64 && cd ../../.. \
  && tar xzf extern/CAENComm-v1.6.0.tgz  -C extern && rm extern/CAENComm-v1.6.0.tgz \
  && cd extern/CAENComm-v1.6.0/lib && ./install_x64 && cd ../../.. \
  && tar xzf extern/CAENDigitizer-v2.17.3.tgz -C extern && rm extern/CAENDigitizer-v2.17.3.tgz \
  && cd extern/CAENDigitizer-v2.17.3/lib && ./install_x64 && cd ../../.. \ 
  && cd /eudaq/eudaq \
  && mkdir -p build \ 
  && cd build \ 
  && cmake .. -DEUDAQ_BUILD_STDEVENT_MONITOR=ON -DEUDAQ_BUILD_PYTHON=ON -DEUDAQ_BUILD_ONLINE_ROOT_MONITOR=ON \
        -DUSER_EUDET_BUILD=ON -DUSER_TLU_BUILD=ON -DUSER_STCONTROL_BUILD=ON -DUSER_CMSIT_BUILD=ON -DUSER_CAEN_DT5742_BUILD=ON \
        -DUSER_ETROC_BUILD=ON -DEUDAQ_LIBRARY_BUILD_TTREE=ON -DBoost_DIR=/eudaq/boost/lib/cmake/Boost-1.77.0 \ 
  && make -j4 install
# STOP ONLY FOR PRODUCTION

#ENV PXARPATH="/eudaq/eudaq/extern/pxar"
ENV LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/eudaq/eudaq/lib"
ENV PYTHONPATH="${PYTHONPATH}:/eudaq/eudaq/lib:/eudaq/eudaq/python"
ENV PATH="${PATH}:/eudaq/eudaq/bin"

COPY initialize_service.sh /usr/bin/initialize_service.sh

# Create a couple of directories needed
RUN mkdir -p /logs && mkdir -p /data
# Add eudaquser, allow to call sudo without password
RUN useradd -md /home/eudaquser -ms /bin/bash -G sudo eudaquser \ 
  && echo "eudaquser:docker" | chpasswd \
  && echo "eudaquser ALL=(ALL) NOPASSWD: ALL\n" >> /etc/sudoers 
# Give previously created folders ownership to the user
RUN chown -R eudaquser:eudaquser /logs && chown -R eudaquser:eudaquser /data \
  && chown -R eudaquser:eudaquser /eudaq
USER eudaquser

