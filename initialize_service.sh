#!/bin/bash

# Wait some time to allow the runControl to wake up

echo "Initializing SERVICE: $1"
if [ "X$1" == "XrunControl" ];
then
    CMD="euRun -a tcp://44000"
elif [ "X$1" == "Xlogger" ];
then
    sleep 3;
    CMD="euLog -r tcp://172.30.128.2:44000"
elif [ "X$1" == "XdataCollector" ];
then
    sleep 6;
    # Change the uid for the data 
    CMD="euCliCollector -n Ex0TgDataCollector -t dc -r tcp://172.30.128.2:44000"
elif [ "X$1" == "XonlineMon" ];
then
    sleep 10;
    CMD="euCliMonitor -n Ex0Monitor -t online_mon -r tcp://172.30.128.2:44000 -a tcp://45002";
elif [ "X$1" == "XTestProducer" ];
then
    sleep 20;
    CMD="euCliProducer -n Ex0Producer -t test_producerpd0 -r tcp://172.30.128.2:44000";
elif [ "X$1" == "XNIProducer" ];
then
    sleep 20;
    CMD="echo 'NOT IMPLEMENTED YET: NIProducer.exe -r tcp://172.30.128.2:44000'";
elif [ "X$1" == "XTLUAIDA" ];
then
    sleep 20;
    # start controlhub for stable AIDA TLU TCP/IP communication
    /opt/cactus/bin/controlhub_start
    /opt/cactus/bin/controlhub_status
    CMD="euCliProducer -n AidaTluProducer -t aida_tlu -r tcp://172.30.148.2:44000";
elif [ "X$1" == "XTLU" ];
then
    # Deal with the permissions for the TLU
    sudo /eudaq/eudaq/bin/EudetTluNoRoot 
    sleep 20;
    CMD="TLUProducer.exe -r tcp://172.20.128.2:44000";
fi

exec ${CMD}
